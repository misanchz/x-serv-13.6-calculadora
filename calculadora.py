#!/usr/bin/python3

import sys

def add(op1, op2):
	return op1 + op2

def sub(op1, op2):
	return op1 - op2

def mult(op1, op2):
	return op1 * op2

def div(op1, op2):
	try:
		return op1 / op2
	except ZeroDivisionError:
		print("Division by zero!")
try:
	if sys.argv[1] == "sum":
		print(add(float(sys.argv[2]), float(sys.argv[3])))
	elif sys.argv[1] == "subtract":
		print(sub(float(sys.argv[2]), float(sys.argv[3])))
	elif sys.argv[1] == "multiply":
		print(mult(float(sys.argv[2]), float(sys.argv[3])))
	elif sys.argv[1] == "divide":
		print(div(float(sys.argv[2]), float(sys.argv[3])))
	else:
		print("Invalid option")
except ValueError:
	print("ValueError: Invalid type for operands")
except IndexError:
	print("IndexError: Bad number of arguments")




